package utils

/**
 * User: andraz
 * Date: 12/11/12
 * Time: 10:23 AM
 */
sealed trait Failure{
  def reason: String
}

object Failure {
  case class BadRequest(reason: String) extends Failure
  case class Unauthorized(reason: String) extends Failure
  case class NotFound(reason: String) extends Failure
  case class Conflict(reason: String) extends Failure
  case class NotAcceptable(reason: String) extends Failure
}
