package utils

import play.api.libs.json._
import com.codahale.jerkson.{Json => Jerkson}
import play.api.mvc.{Results, BodyParser}
import play.api.mvc.BodyParsers.parse.DEFAULT_MAX_TEXT_LENGTH
import play.api.libs.iteratee.{Done, Iteratee, Traversable}
import play.api.Play
import play.api.libs.iteratee.Input.{El, Empty}

/**
 * User: andraz
 * Date: 11/24/12
 * Time: 5:12 PM
 */
trait JsonHelpers extends {
  implicit def any2json[A: Writes](a: A) = Json.toJson(a)

  //easier way to create a json literal
  def JsonObject(xs: (String, JsValue)*) = JsObject(xs)

  object parsers{
    def json[A : Manifest]: BodyParser[A] = BodyParser("json, maxLength=" + DEFAULT_MAX_TEXT_LENGTH) { request =>
      Traversable.takeUpTo[Array[Byte]](DEFAULT_MAX_TEXT_LENGTH).apply(Iteratee.consume[Array[Byte]]().map { bytes =>
        scala.util.control.Exception.allCatch[A].either {
          Jerkson.parse[A](new String(bytes, request.charset.getOrElse("utf-8")))
        }.left.map { e =>
          (Play.maybeApplication.map(_.global.onBadRequest(request, "Invalid Json")).getOrElse(Results.BadRequest), bytes)
        }
      }).flatMap(Iteratee.eofOrElse(Results.EntityTooLarge))
        .flatMap {
        case Left(b) => Done(Left(b), Empty)
        case Right(it) => it.flatMap {
          case Left((r, in)) => Done(Left(r), El(in))
          case Right(a) => Done(Right(a), Empty)
        }
      }
    }
  }
}
