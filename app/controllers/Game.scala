package controllers

import play.api._
import libs.concurrent.Akka
import play.api.mvc._
import play.api.libs.json._
import utils.Failure
import play.api.Play.current
import com.codahale.jerkson.Json.generate

import models._

/**
 * User: andraz
 * Date: 11/24/12
 * Time: 2:46 PM
 */
object Game extends Controller with utils.JsonHelpers {

  def jsonFail(reason: String) = JsonObject(
    "reason" -> reason
  )

  //200 ok
  def ok(any: Any) = Ok(generate(any)).as("application/json")

  def fail(f: Failure) = (f match {
    case resp: Failure.BadRequest => BadRequest(generate(resp))
    case resp: Failure.Unauthorized => Unauthorized(generate(resp))
    case resp: Failure.NotFound => NotFound(generate(resp))
    case resp: Failure.Conflict => Conflict(generate(resp))
    case resp: Failure.NotAcceptable => NotAcceptable(generate(resp))
  }) as "application/json"

  def create = Action(parsers.json[CreateGame]){ request =>
    models.Game.create(request.body) match {
      case Right(game) => ok(game)
      case Left(reason) => fail(reason)
    }
  }

  def join = Action(parsers.json[JoinGame]){ request =>
    val JoinGame(gameId, password) = request.body
    models.Game.join(gameId, password) match {
      case Right(game) => ok(game)
      case Left(reason) => fail(reason)
    }
  }

  def update(id: String) = Action(parsers.json[UserUpdate]){ request =>
    val UserUpdate(user, lat, lon) = request.body
    Async{
      Akka.future(models.Game.update(id, user, lat, lon)) map {
        case Some(game) => ok(game.users)
        case None => fail(Failure.NotFound("game not found"))
      }
    }
  }

  def all = Action{
    ok(models.Game.all)
  }

  def fake = Action(parsers.json[UserUpdate]){ request =>
    val UserUpdate(_, lat, lon) = request.body
    //0.009 deg is about 1km
    Ok(generate(
      (1 to 5) map { _ =>
      UserState(
        math.random.toString.substring(2),
        Some((lat-0.009+math.random*0.0018)),
        Some((lon-0.009+math.random*0.0018))
      )
    }
    )) as "application/json"
  }
}
