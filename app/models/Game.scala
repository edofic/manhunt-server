package models

import scala.Either
import utils.{JsonHelpers, Failure}
import play.api.Play.current
import com.novus.salat._
import com.novus.salat.dao._
import com.novus.salat.global._
import com.mongodb.casbah.Imports._
import se.radley.plugin.salat._
import play.api.libs.json.Writes

/**
 * User: andraz
 * Date: 11/24/12
 * Time: 2:23 PM
 */
case class Game(_id: ObjectId = new ObjectId,
                game_name: String,
                password: String,
                latitude: Double,
                longitude: Double,
                game_type: String,
                max_players: Int,
                date: Long,
                users: List[UserState])

object Game{
  object dao extends SalatDAO[Game, ObjectId](collection = mongoCollection("games"))

  def create(req: CreateGame): Either[Failure, GameCreated] = {
    val user = new ObjectId().toString
    val users = List(UserState(user))
    val game = Game(
      game_name= req.game_name,
      password= req.password,
      latitude = req.latitude,
      longitude = req.longitude,
      game_type = req.game_type,
      max_players = req.max_players,
      date = req.date,
      users=users
    )

    dao.insert(game) match {
      case Some(id) => Right(GameCreated(id.toString, user))
      case None => Left(Failure.Conflict("mongo error"))
    }
  }

  def join(gameId: String, password: String): Either[Failure, GameJoined] = {
    val user = new ObjectId().toString
    val id = new ObjectId(gameId)
    val game = dao.findOneById(id)
    if(game.isEmpty){
      Left(Failure.NotFound("game doesn't exist"))
    } else {
      val gameSome = game.get
      if(gameSome.password != password)
        Left(Failure.Unauthorized("wrong password"))
      else{
        val state = grater[UserState].asDBObject(UserState(user))
        dao.update(
          MongoDBObject("_id" -> id),
          MongoDBObject("$addToSet" -> MongoDBObject(
            "users" -> state
          ))
        )
        Right(GameJoined(user))
      }
    }
  }

  def getState(gameId: String) = dao.findOneById(new ObjectId(gameId))

  def update(gameId: String, userId: String, lat: Double, lon: Double): Option[Game] = {
    dao.update(
      MongoDBObject("_id"->new ObjectId(gameId), "users.user_id" -> userId),
      MongoDBObject(
        "$set" -> MongoDBObject("users.$.lat" -> lat, "users.$.lon" -> lon)
      )
    )
    val userState = UserState(userId, Some(lat), Some(lon))
    UserState.dao.insert(userState)
    getState(gameId)
  }

  def all: Seq[GameMeta] = {
    val iterator = for{
      game <- dao.find(MongoDBObject())
    } yield GameMeta(game._id.toString,
      game.game_name,
      game.latitude,
      game.longitude,
      game.game_type,
      game.max_players,
      game.date)
    iterator.toList
  }
}