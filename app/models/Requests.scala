package models

/**
 * User: andraz
 * Date: 12/22/12
 * Time: 12:41 AM
 */
case class CreateGame(game_name: String,
                      password: String,
                      latitude: Double,
                      longitude: Double,
                      game_type: String,
                      max_players: Int,
                      date: Long)

case class JoinGame(game_id: String, password: String)
case class UserUpdate(user_id: String, latitude: Double, longitude: Double)