package models

import play.api.Play.current
import com.novus.salat._
import com.novus.salat.dao._
import com.novus.salat.global._
import com.mongodb.casbah.Imports._
import se.radley.plugin.salat._

/*
 * User: andraz
 * Date: 12/12/12
 * Time: 10:11 AM
 */

case class Fail(reason: String)
case class GameCreated(game_id: String, user_id: String)
case class GameJoined(user_id: String)

case class UserState(user_id: String, lat: Option[Double] = None, lon: Option[Double] = None)
object UserState {
  object dao extends SalatDAO[UserState, ObjectId](collection = mongoCollection("updates"))
}

case class GameMeta(gameId: String,
                    name: String,
                    latitude: Double,
                    longitude: Double,
                    game_type: String,
                    max_players: Int,
                    date: Long)